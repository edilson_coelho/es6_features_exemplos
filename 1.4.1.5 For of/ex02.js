const nomes = ['ES5', 'Node.js', 'ES6'];

for (let nome of nomes) {
    console.log(nome); /* resultado
                         ES5
                         Node.js 
                         ES6
                      */
}

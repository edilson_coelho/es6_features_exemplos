const dados = new Set();
const contribuinte =
    {
        nome: 'Edilson',
        tipo: 'S'
    };
const frameworks = ['laravel', 'express.js', 'hapi.js'];

dados.add('node.js');
dados.add('traceur');
dados.add(contribuinte);
dados.add(frameworks);

// iterar a coleção e obter das informação adicionadas
for (let item of dados) {
    console.log(item);
}

/*
	Resultado
    	node.js
		traceur
		{ "nome": "Edilson", "tipo": "S" }
		[ "laravel", "express.js", "hapi.js" ]
*/

// obter o total de elementos adicionados na coleção
console.log(dados.size); // resultado 4

// verificar se uma determinada informação já foi adicionada
console.log(dados.has('node.js')); // resultado true
console.log(dados.has('koa.js')); // resultado false


// remover um elemento da coleção
dados.delete('traceur');

/* 
	obter o total de elementos adicionados na coleção após eliminação
	do elemento 'traceur'
*/
console.log(dados.size); // resultado 3

// global scope

for (var i = 0; i < 5; i++) {
    console.log(i); // resultado usando _var 0, 1, 2, 3, 4
}

console.log(i); // resultado usando _var 5

for (let y = 0; y < 5; y++) {
    console.log(y); // resultado usando _let 0, 1, 2, 3, 4
}

console.log(y); // resultado usando _let 'variável "y" não foi definida'

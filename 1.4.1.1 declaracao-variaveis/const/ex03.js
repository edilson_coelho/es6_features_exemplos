const produto =
    {
        nome: 'const',
        quantidade: 12
    }

console.log(produto.nome); // resultado 'const'

produto.nome = 'novo nome';
console.log(produto.nome); // resultado 'novo nome'

produto = {}; // erro: produto é apenas para leitura

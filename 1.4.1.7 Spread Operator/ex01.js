const valores = [1, 2];

function soma(x, y) {
    return (x + y);
}

console.log(soma(...valores)); // resultado 3

/*
 Obs: A velha maneira de fazer...
    
 soma(valores[0], valores[1]); // resultado 3
*/

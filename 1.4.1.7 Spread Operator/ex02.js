const nomes = ['Node.js', 'ES6', 'Express.js'];

function toString() {
    const outrosNomes = [ 'Hapi.js', 'Koa.js', ...nomes ];
    console.log(outrosNomes);
}

toString(); // resultado, [ "Hapi.js", "Koa.js", "Node.js", "ES6", "Express.js" ]

/*
 Obs: A velha maneira de fazer...
    
 const outrosNomes = [ 'Hapi.js', 'Koa.js', nomes[0], nomes[1], nomes[2] ];
*/

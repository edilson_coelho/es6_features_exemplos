// forma antiga
function getProdutos1(produtos) {
    console.log('antigo: ', produtos);
}
getProdutos1(['a', 'b', 'c']); // resultado, antigo:  [ "a", "b", "c" ]


// nova forma
const getProdutos2 = produtos => console.log('novo 01: ', produtos);
getProdutos2(['a', 'b', 'c']); // resultado, novo 01:  [ "a", "b", "c" ]

// outra representação

const getProdutos3 = (produtos) => console.log('novo 02: ', produtos);
getProdutos3(['a', 'b', 'c']); // resultado, novo 02:  [ "a", "b", "c" ]

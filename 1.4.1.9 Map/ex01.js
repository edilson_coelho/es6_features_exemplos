const contribuintes = new Map();

contribuintes.set('00000001', { nome: 'C1', tipo: 'S' });
contribuintes.set('00000002', { nome: 'C2', tipo: 'C' });
contribuintes.set('00000033', { nome: 'C3', tipo: 'N' });

// consultar dados com base na chave ( key: 00000001 )
console.log(contribuintes.get('00000001')); /*
                                                Resultado                                    
                                                { "nome": "C1", "tipo": "S" }
                                            */

// consultar todas as chaves
for (let key of contribuintes.keys()) {
    console.log(key);
} /*
  Resultado (keys)
    00000001
    00000002
    00000033
 */


// consultar todos os valores
for (let value of contribuintes.values()) {
    console.log(value);
} /*
  Resultado (valores)
    { "nome": "C1", "tipo": "S" }
    { "nome": "C2", "tipo": "C" }
    { "nome": "C3", "tipo": "N" }
 */

// verificar se as chaves existem
console.log(contribuintes.has('00000001')); // resultado true
console.log(contribuintes.has('00000111')); // resultado false

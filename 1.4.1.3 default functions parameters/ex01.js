function soma(x, y = 1) {
    return (x + y);
}

let resultado = soma(4);
console.log(resultado); // resultado 5 ( x =  4, y = 1)
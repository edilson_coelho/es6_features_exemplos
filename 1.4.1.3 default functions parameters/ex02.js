function soma(x = 4, y = 1) {
    return (x + y);
}

console.log(soma()); // resultado 5 (x = 4, y = 1)
console.log(soma(3)); // resultado 4 (x = 3, y = 1)
// for..of
const nomes = ['ES6', 'Node.js'];

for (let nome of nomes) {
    console.log(nome); /* resultado
                         ES6
                         Node.js                          
                      */
}

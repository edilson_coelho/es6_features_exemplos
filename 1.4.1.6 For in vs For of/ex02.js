// for..in
const _nomes = ['ES6', 'Node.js'];

for (let nome in _nomes) {
    console.log(nome); /* resultado
                         0
                         1                          
                      */
}

const pessoa =
    {
        nome: 'Edilson',
        apelido: 'Coelho'
    };

const { nome, apelido } = pessoa;

console.log(nome); // resultado Edilson
console.log(apelido); // resultado Coelho

/*
Obs: A maneira antiga de fazer...

console.log(pessoa.nome); // resultado Edilson
console.log(pessoa.apelido); // resultado Coelho
*/

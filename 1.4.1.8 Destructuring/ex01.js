const nomeApelido = ['Edilson', 'Coelho'];

const [nome, apelido] = nomeApelido;

console.log(nome); // resultado Edilson
console.log(apelido); // resultado Coelho

/*
    Obs: A maneira antiga de fazer...
    
    console.log(nomeApelido[0]);
    console.log(nomeApelido[1]);
*/
